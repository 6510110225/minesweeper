import random

def random_bombs(rows, cols, bomb_number):
    mine = [[0 for j in range(cols)] for i in range(rows)]
    bombs_placed = 0
    while bombs_placed < bomb_number:
        row = random.randint(0, rows-1)
        col = random.randint(0, cols-1)
        if mine[row][col] == 0:
            mine[row][col] = -1
            bombs_placed += 1
    return mine

if __name__ == "__main__":
    mine = random_bombs(5, 5, 5)
    for row in mine:
        print(row)